# OpenML dataset: cars

https://www.openml.org/d/44835

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

**Data Description**

Data frame of the suggested retail prices (column *Price*) and various characteristics of each car.

For this data set, a representative sample of over eight hundred 2005 GM cars were selected, then retail prices were calculated from the tables provided in the 2005 Central Edition of the Kelly Blue Book.

**Attribute Description**

All features describe different self-explanatory characteristics for the cars.

1. *Price* - target feature
2. *Mileage*
3. *Cylinder*
4. *Doors*
5. *Cruise*
6. *Sound*
7. *Leather*
8. *Buick*
9. *Cadillac*
10. *Chevy*
11. *Pontiac*
12. *Saab*
13. *Saturn*
14. *convertible*
15. *coupe*
16. *hatchback*
17. *sedan*
18. *wagon*

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/44835) of an [OpenML dataset](https://www.openml.org/d/44835). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/44835/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/44835/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/44835/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

